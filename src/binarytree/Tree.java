package binarytree;

public class Tree {
    private Node root;
    private String name;

    public Tree(String name, Node root) {
        this.name = name;
        this.root = root;
    }

    public Tree setRoot(Node node) {
        this.root = node;
        return this;
    }

    public Node getRoot() {
        return this.root;
    }

    public Tree setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Tree addNode(Node node) {
        if(this.root == null) {
            this.root = node;
        } else {
            this.root.addNode(node);
        }

        return this;
    }

    public void inOrder(Node node) {
        if(node.getLeft() != null) this.inOrder(node.getLeft());
        System.out.println(node.getKey());
        if(node.getRight() != null) this.inOrder(node.getRight());
    }

}
