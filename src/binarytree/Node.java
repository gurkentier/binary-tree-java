package binarytree;

public class Node {
    private Node left;
    private Node right;
    private int key;

    public Node(int key) {
        this.key = key;
    }

    public Node setLeft(Node node) {
        this.left = node;
        return this;
    }
    public Node getLeft() {
        return this.left;
    }

    public Node setRight(Node node) {
        this.right = node;
        return right;
    }

    public Node getRight() {
        return this.right;
    }

    public Node setKey(int key) {
        this.key = key;
        return this;
    }

    public int getKey() {
        return this.key;
    }

    public Node addNode(Node node) {
        if(this.getKey() > node.getKey()) {
            if(this.getLeft() != null) {
                this.getLeft().addNode(node);
            } else {
                this.setLeft(node);
            }
        } else {
            if(this.getRight() != null) {
                this.getRight().addNode(node);
            } else {
                this.setRight(node);
            }
        }

        return this;
    }
}
